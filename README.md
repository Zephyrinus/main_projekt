# Main_projekt


---
title: 'Practice'

subtitle: 'In this repository I am learning by doing'

authors: ['Romulus Prundis \<rvpr37603@edu.ucl.dk\>']

date: \today
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---


# Main_project

In this repository you can find different projects for begginers that I learned from them, most of the sources are from Youtube videos or https://www.w3schools.com/.

The following programs are: Quizgame, Guess the number, Password Manager, Webscraping, Adventure (write your own) and find Phone number.
