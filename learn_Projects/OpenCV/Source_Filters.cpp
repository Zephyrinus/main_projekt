#pragma once
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <stdint.h>



using namespace cv;
using namespace std;


int main(int argv, char** argc)
{
	Mat original = imread("OpenCV_Resources/boat.jpg", IMREAD_COLOR);
	Mat modified = imread("OpenCV_Resources/boat.jpg", IMREAD_COLOR);

	for (int r = 0; r < modified.rows; r++)
	{
		for (int c = 0; c < modified.cols; c++)
		{

			modified.at<cv::Vec3b>(r, c)[1] = modified.at<Vec3b>(r, c)[1] * 0;
		}
	}

	imshow("Original", original);
	imshow("Modified", modified);

	imwrite("outputColor1.jpg", modified);
	waitKey();

}