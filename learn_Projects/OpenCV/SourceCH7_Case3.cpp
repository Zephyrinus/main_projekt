#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <filesystem>


using namespace cv;
using namespace std;

/// /////////// Importing Images  ///////
Mat imgGray, imgBLur, imgCanny, imgDil, imgErode;

void getContours(Mat imgDil, Mat img) {

	//{{Point(20,30),Point(50,60)},
	vector<vector<Point>> contours;
	vector<Vec4i>hierarchy;

	findContours(imgDil, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	//drawContours(img, contours, -1, Scalar(255, 0, 255), 5);


	for(int i = 0; i< contours.size(); i++)
	{
		int area = contourArea(contours[i]);
		cout << area << endl;

		vector<vector<Point>> conPoly(contours.size());
		vector<Rect> boundRect(contours.size());//the varialbe that we introduce the vector
		string objectType;

		if (area > 2000)
		{
			float peri = arcLength(contours[i], true);
			approxPolyDP(contours[i], conPoly[i], 0.02 * peri, true);
			
			cout << conPoly[i].size() << endl;
			boundRect[i] = boundingRect(conPoly[i]); //function we use to call to find the bounding rectangle around it
			

			int objCor = (int)conPoly[i].size();

			if (objCor == 3) { objectType = "Tri"; }
			if (objCor == 4) {

				float aspRatio = (float)boundRect[i].width / boundRect[i].width / (float)boundRect[i].height;
				cout<< aspRatio <<endl;
				if (aspRatio > 0.95 && aspRatio < 1.05) { objectType = "Square"; }
				else {
					objectType = "Rect";
				}
			}
			if (objCor == 5) { objectType = "Circle"; }

			drawContours(img, conPoly, i, Scalar(255, 0, 255), 5);
			rectangle(img, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 255, 0), 5);
			putText(img, objectType, { boundRect[i].x,boundRect[i].y - 5 },FONT_HERSHEY_PLAIN,1,Scalar(0, 69, 255), 2);
		}
	}
}

void main() {

	string path = "OpenCV_Resources/signs.jpg";
	Mat img = imread(path);

//we will use Kenny edge detector to find the edges, and after we will find the counter points
	//Preprocessing the image
	cvtColor(img, imgGray, COLOR_BGR2GRAY);
	GaussianBlur(imgGray, imgBLur, Size(3, 3), 3, 0);
	Canny(imgBLur, imgCanny, 25, 75);
	Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
	dilate(imgCanny, imgDil, kernel);

	getContours(imgDil, img);

	imshow("Image", img);
	imwrite("OpenCV_Resources/signs.jpg", img);
	/*imshow("Image Gray", imgGray);
	imshow("Image Blue", imgBLur);
	imshow("Image Canny", imgCanny);
	imshow("Image Dill", imgDil);//we use dialetion in case some shapes have space, not fully connected*/
	waitKey(0);

}

