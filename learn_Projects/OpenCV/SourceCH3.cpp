#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

/// /////////// Resize and Crop  ///////



void main() {

	string path = "OpenCV_Resources/dragon.png";
	Mat img = imread(path);
	Mat imgResize, imgCrop;

	int width = img.cols;
	int height = img.rows;
	cout << "Width of image in pixels: " << width;
	cout << "Height of image in pixels: " << height;
	

	cout << img.size() << endl;
	resize(img, imgResize, Size(640,480));
	resize(img, imgResize, Size(), 0.5, 0.5);

	Rect roi(200, 200, 300, 300);
	imgCrop = img(roi);
	
	imshow("Image", img);
	imshow("Image Resize", imgResize);
	imshow("Image Crop", imgCrop);

	waitKey(0);

}

