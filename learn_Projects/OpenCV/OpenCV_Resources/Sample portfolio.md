 ## Computer Vision Portfolio Template

*Author name:* Romulus Prundis
*Education*: IT technology

## Table of contents

* Introduction
In Denmark there are many private proprietes of companies that are being illegally passed or people parking their cars because there is no sign paying for parking.
Based on this problem Computer vision can come handy. "OpenCV (Open Source Computer Vision Library) is a library of programming functions mainly aimed at real-time computer vision. Originally developed by Intel, it was later supported by Willow Garage then Itseez (which was later acquired by Intel)." Source: https://opencv.org/about/
By using it, one can process images and videos to identify objects, faces, or even handwriting of a human. When it integrated with various libraries, such as NumPy, python is capable of processing the OpenCV array structure for analysis. To Identify image pattern and its various features we use vector space and perform mathematical operations on these features. 
This project is in connection with my internship for the 3rd semester where i have to use a microcontroller esp32 with the camera module to create it a webserver that can serve as a camera that can be accessed from different networks (remotely). The OpenCV open-source librabry comes in hand as instead of having someone staring at the camera 24/7 we can use Computer Vision for people detection and to trigger an sms every time the algorithm identified the structure of a person.
This is why in this portfolio i will present a human detection use case and car detection use case.

* Problem statements
	* Use case 1
	* Use case 2
	* Use case 3
* Methodology and process
	* Use case 1
	* Use case 2
	* Use case 3
* Results and final evaluation
	* Use case 1
	* Use case 2
	* Use case 3
* Appendix

Porfolio has to include:
• Preprocessing and edge detection
• Shape detection
• Filtering techniques
• One use case involving videos

An extra criteria also exists for your personal use case, namely that it has to involve a detection algorithm,
 e.g. Harris Corner detection, or SimpleBlobDetector, or SIFT keypoints, or others (these are handled next week).
## Introduction

## Methodology and process

### Use case 1 - identifying humans in pictures

I chose Gaussian blur to filter out the noise, as the edge detection algorithm detected too many leaves. Gaussian blur makes all pixels in the image be an average of the neighbouring pixel values and itself, and thereby smooths out the image.

I chose the  histograms of oriented gradients (HOG) algorithm because it speeds up the detetion process. As the topic of this project is people/human detection in a private space, the HOG algorthim can avoid the large number of double calculations of the original HOG image that is used as dataset s used to describe the contour of the human body in real time. Based on the HOG features, using a training sample set consisting of scene images of a bulk port, a support vector machine (SVM) classifier combined with the AdaBoost classifier is trained to detect human. Finally, the results of the human detection experiments on Tianjin Port show that the accuracy of the proposed optimized algorithm has roughly the same accuracy as a traditional algorithm, while the proposed algorithm only takes 1/7 the amount of time.
Source: https://www.hindawi.com/journals/mpe/2014/386764/

Before blurring:

![](https://i.imgur.com/W5NIamZ.jpg)

After blurring with a 7 by 7 kernel and converting to grayscale:

![](https://i.imgur.com/elqDUXr.jpg)


Now that we have reduced the noise in the image, we can extract edges with the Canny edge detection algorithm.

### Use case 2 - identifying humans in a video

......

## Results and evaluation

### Use case 1

We found that gaussian blur with a kernel size of x by x 

### Use case 2


.....

## Appendix

Image with 3x3 kernel that did not smooth enough:

![](https://i.imgur.com/t2Z1WI7.jpg)

Image with 19x19 kernel that smoothed so much the details of the object of interest got too out of focus:

![](https://i.imgur.com/XBqozlc.jpg)
