#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

/// /////////// Draw Shapes and Text  ///////

void main() {

	//blank image
	Mat img(512, 512, CV_8UC3, Scalar(255, 250, 200));

	//circle(img, Point(256, 256), 155, Scalar(0, 30, 255),15);
	circle(img, Point(256, 256), 155, Scalar(0, 30, 255), FILLED);
	rectangle(img, Point(130, 226), Point(386, 286), Scalar(255, 255, 255), FILLED);
	line(img, Point(130, 296), Point(382, 296), Scalar(255, 255, 255), 2);

	putText(img, "Ave Romulus", Point(137, 262), FONT_HERSHEY_COMPLEX, 0.99, Scalar(0, 69, 100), 2);
	imshow("Image", img);

	waitKey(0);

}

