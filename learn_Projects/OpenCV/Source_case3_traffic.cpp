#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

/// /////////// Basic Images functions  ///////
int main()
{
    // Reading image
    string path = "OpenCV_Resources/traffic.jpg";
    Mat img = imread(path);
    // Display original image
    imshow("original Image", img);
    waitKey(0);

    // Convert to graycsale because you do not need color information to detect edges.
    Mat img_gray;
    cvtColor(img, img_gray, COLOR_BGR2GRAY);
    // Blur the image for better edge detection, this is done  to reduce the noise in the image
    Mat img_blur;
    GaussianBlur(img_gray, img_blur, Size(3, 3), 0);

    // Sobel edge detection, detects edges that are marked by sudden changes in pixel intensity,
    Mat sobelx, sobely, sobelxy;
    Sobel(img_blur, sobelx, CV_64F, 1, 0, 5);
    Sobel(img_blur, sobely, CV_64F, 0, 1, 5);
    Sobel(img_blur, sobelxy, CV_64F, 1, 1, 5);
    // Display Sobel edge detection images
    imshow("Sobel X", sobelx);
    waitKey(0);
    imshow("Sobel Y", sobely);
    waitKey(0);
    imshow("Sobel XY using Sobel() function", sobelxy);
    waitKey(0);

    // Canny edge detection
    Mat edges;
    Canny(img_blur, edges, 100, 200, 3, false);
    // Display canny edge detected image
    imshow("Canny edge detection", edges);
    imwrite("OpenCV_Resources/traffic_Case-3.jpg", img);
    waitKey(0);

    destroyAllWindows();
    return 0;
}