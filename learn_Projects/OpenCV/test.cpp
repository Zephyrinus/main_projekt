#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

void main() {

	string path = "/OpenCV_Resources/justdoit.avi";
	VideoCapture cap(path);
	Mat video;

	while (true) {

		cap.read(video);

		imshow("Mvideo", video);
		waitKey(20);
	}
