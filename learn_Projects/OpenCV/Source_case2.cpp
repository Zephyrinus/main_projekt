#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>
#include <iostream>

using namespace cv;
using namespace std;

/// /////////// Importing Images  ///////

void main() {

	string path = "OpenCV_Resources/face.jpg";
	Mat img = imread(path);

	CascadeClassifier faceCascade;
	faceCascade.load("OpenCV_Resources/haarcascade_frontalface_default.xml");

	//checking if the cascade file is loaded
	if (faceCascade.empty()) {cout << "XML file not loaded" << endl;}

	vector<Rect> faces;
	faceCascade.detectMultiScale(img, faces, 1.1, 10);

	for (int i = 0; i < faces.size();i++)
	{
		rectangle(img, faces[i].tl(), faces[i].br(),  Scalar(255, 0,255), 3);
	}

	imshow("Image", img);
	imwrite("OpenCV_Resources/Jane_Case-2.jpg", img);
	waitKey(0);

}

