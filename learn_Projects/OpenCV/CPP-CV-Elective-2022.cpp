#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include "basic.h"
#include "detectors.h"
#include "analysis.h"
#include "thresholding.h"
#include "chains.h"
#include "filters.h"
#include "ex1.h"
#include "keypoint_matching.h"

#include <opencv2/core/utils/logger.hpp>

using namespace cv;
using namespace std;

int main(int argc, char** argv) {
	utils::logging::setLogLevel(utils::logging::LogLevel::LOG_LEVEL_SILENT);
	Mat image = imread("OpenCV_Resources/pill.jpg");
	Mat temp = imread("OpenCV_Resources/pills.jpg");
	sift_keypoints(image);
}