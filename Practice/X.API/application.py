from flask import Flask
app = Flask(__name__)
from flask_sqlalchemy import SQLAlchemy


app.config['SQLAlchemy_DATABASE_URI'] = 'sqlite:///data.db'  #create sql database 

db = SQLAlchemy(app)

class Wines(db.Model):     # we use the sql from flask here
    id = db.Column(db.Integer, primary_key=True)    
    name =  db.Column(db.String(80), unique = True, nullable=False)
    description = db.Column(db.String(120))

    def__repr__(self):  # we use self that refers tot he object
        return f"{self.name} - {self.description}"



@app.route('/')
def index():
    return "Hej, I'm working!"


@app.route('/wines')
def get_wines():

    return wines