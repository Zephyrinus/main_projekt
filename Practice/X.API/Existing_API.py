import requests
import json

respon = requests.get("http://api.stackexchange.com/2.3/questions?order=desc&sort=activity&site=stackoverflow")

print(respon)

for data in respon.json()["items"]:
    if data["answer_count"] == 0:   #we set to 0 so check if no one answer to those questions
        print(data["title"])
        print(data["link"])
    else:
        print("skipped")
    print()
