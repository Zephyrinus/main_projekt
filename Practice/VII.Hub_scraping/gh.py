# Resource https://pygithub.readthedocs.io/en/latest/github_objects/Commit.html
# for counting lines of code, count per commit
from github import Github
import requests
import json
import matplotlib.pyplot as plt

g = Github("#--add key here--")

repo = g.get_repo("microsoft/vscode")
# print(repo.get_contents("/"))
print(repo.get_stargazers().totalCount)       #print how many people have starred the repo

#print(repo.get_commits())
#print(repo.get_pulls())

#for commit in repo.get_commits():
#	print(commit.stats.total)
	
# for pull in repo.get_pulls():        #to count for pull requests
#	print(pull.user)

#print(repo.get_clones_traffic())        #must have writing access

f = open("gh.json", "w")   #r is for reading, a is writing, and w is overwriting

dayAverage = 0

req = requests.get("https://api.github.com/repos/microsoft/vscode/commits").json()

username = input("Which user do you want daily average commits for?\n")

for d in req:
	for item in d.items():
		if item[0] == 'sha':
			print(f"The commit {item[1]}")	# get sha # all repositories
		if item[0] == 'commit':
			print(f" by {item[1]['author']['name']}")
			print(f" was pushed at {item[1]['author']['date']}")
			print(f" with message {item[1]['message']}")
			if item[1]['author']['name'] == username:
				f.write(json.dumps(item[1]['author']['date']) + "	" + json.dumps(item[1]['message']) + "\n")
			if item[0] == 'commit':
				dayAverage += 1
	print("-------------")
	# commits per day average
#print(dayAverage)
print(f"Average commits per day: {dayAverage/len(item[0])}")
	#f.write(json.dumps())        # takes the json and attaches it there, dumps tell you want to put a json formatted string in a file



'''
for item in req[0].items():
	if item[0] == 'sha':
		print(item[1])	

'''

# print(req[0]['sha'])

#req[0]["commit"]["author"]["date"]        #we add 0 because in list we need to index it
#req = f'The commit {f[0]["sha"]} by {f[0]["commit"]["author"]["name"]} was pushed at datetime {f[0]["commit"]["author"]["date"]}'
#print(req)

#commits = req


#commit_one = ""
#for commit in commits:
#	commit_one += commit[0]
#print(commit_one)

