import sys
import clipboard
import json

saved_data = "clipboard.json"
#print(sys.argv[1:])
#clipboard.copy("blablalbala")
#data = clipboard.paste()  #paste data from clipbaord to data variable
#print(data)

def save_data(filepath, data):
    with open(filepath, "w") as f:
        json.dump(data, f)

def load_data(filepath):
    try:
        with open(filepath, "r") as f:
            data = json.load(f)
            return data
#save_items("test.json", {"key": "values"})
    except:
        return {}



if len(sys.argv) == 2:
    command = sys.argv[1]
    data = load_data(saved_data)

    if command =="save":
        key = input("ENter a key: ")
        data[key] = clipboard.paste()
        save_data(saved_data, data)
        print("you just saved the data")
    elif command =="load":
        key = input("ENter a key: ")
        if key in data:
            clipboard.copy(data[key])
            print("data copied to the clipboard")
        else:
            print("key does not exist")
    elif command =="list":
        print(data)
    else:
        print("unknown command")
print("pass exactly one command.")

## use the terminal to start it and write > python3 .\xmulticlipboard.py load/save