Terminal comand
pip install pipenv

#install django in the virtual environment
pipenv install django 

#activate the virtual environemnt
pipenv shell

#start a new project
django-admin
django-admin startproject >name of the project<
ex: django-admin startproject storefront
django-admin startproject storefront .

python manage.py
python manage.py runserver

#to create an app
python manage.py startapp >the name we want ex:playground
ex:python manage.py startapp playground