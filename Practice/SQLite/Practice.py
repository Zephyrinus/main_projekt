import sqlite3
from employee import Employee


 conn = sqlite3.connect('employee.db') 
 #sqlite3.connect(':memory') 

cur = conn.cursor()

cur.execute("""CREATE TABLE employees(
            first text,
            last text,
            pay integer
            )""")

def insert_emp(emp):
    with conn:
        cur.execute("INSERT INTO employees VALUES(:first, :last, :pay)", {'first': emp.first, 'last': emp.last, 'pay': emp.pay})
   


def get_emps_by_name(lastname):
    cur.execute("SELECT * FROM employees WHERE last = :last", {'last': lastname})
    return cur.fetchall()


def update_pay(emp, pay):
    with conn:
        cur.execute("""UPDATE employees SET pay = :pay
                    WHERE first = :first AND last = :last""",
                  {'first': emp.first, 'last': emp.last, 'pay': pay})


def remove_emp(emp):
    with conn:
        cur.execute("DELETE from employees WHERE first = :first AND last = :last",
                  {'first': emp.first, 'last': emp.last})

emp_1 = Employee('Vali', 'Vijelie', 1000)
emp_2 = Employee('Costi', 'Ionita', 2000)

'''
cur.execute("INSERT INTO employees VALUES(?, ?, ?)", (emp_1.first, emp_1.last, emp_1.pay))
#to be sure the changes in the table are made with ION, the ? are tuples
conn.commit()

cur.execute("INSERT INTO employees VALUES(:first, :last, :pay)", ('first': emp_2.first, 'last': emp_2.last, 'pay': emp_2.pay))   # ':'are dictionaries, the key are the placeholder
conn.commit()



#to querry/search database we use the code below, cur.fetchall() will return all characters, fetchone is just for one
cur.execute("SELECT * FROM employees WHERE last = ?", ('Ratiu',))
print(cur.fetchall())

cur.execute("SELECT * FROM employees WHERE last = :last", {'last': 'Ion'})
print(cur.fetchall())
'''
conn.commit()

insert_emp(emp_1)
insert_emp(emp_2)

emps = get_emps_by_name('vali')
print(emps)

conn.close()
