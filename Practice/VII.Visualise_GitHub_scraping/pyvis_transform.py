edges_df = pd.read_csv(f'top_{TOPIC}' + '_edge_list.csv')
edges_df = edges_df[edges_df['Target'].str.contains("leet|python")] # getting topics related to Leet and Python
edges_df['Source_Weight'] = edges_df.apply(lambda row: repo_list_filtered_df[repo_list_filtered_df['Full name']==row.Source]["Number of stars"].item(), axis=1)
edges_df['Target_Weight'] = edges_df.apply(lambda row: topics_counter.get(row.Target)*100000, axis=1)

github_net = Network(height='1000px', width='50%', bgcolor='#222222', directed=False, font_color=True, notebook=True)
github_net.show_buttons(filter_=['physics','selection','renderer','interaction','manipulation'])
github_net.set_edge_smooth('dynammic')

github_net.force_atlas_2based(overlap= 0.5)
github_data = edges_df

sources = github_data['Source']
targets = github_data['Target']
source_weights = github_data['Source_Weight']
target_weights = github_data['Target_Weight']

edge_data = zip(sources, targets, source_weights, target_weights)

for e in edge_data:
    src, dst, src_w, dst_w = e
    github_net.add_node(src, src, title=src, size=src_w, group=1)
    github_net.add_node(dst, dst, title=dst, size=dst_w, group=2)
    github_net.add_edge(src, dst)

neighbor_map = github_net.get_adj_list()

# add neighbor data to node hover data
for node in github_net.nodes:
    node['title'] += ' Neighbors:<br>' + '<br>'.join(neighbor_map[node['id']])
    node['value'] = len(neighbor_map[node['id']])

github_net.show('github_repo_topic.html')