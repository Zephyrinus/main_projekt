from github import Github
import pprint
import pandas as pd
import json

g = Github()
TOPIC = "interview-practice"

# search by topic
all_repo = g.search_repositories(f'topic:{TOPIC}')
print(all_repo.totalCount) # returns 678 repositories