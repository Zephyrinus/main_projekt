def get_repo_dict(repo):
    return {"Full name": repo.full_name,
            "Description": repo.description,
            "Date created": repo.created_at,
            "Date of last push": repo.pushed_at,
            "Home Page": repo.homepage,
            "Language": repo.language,
            "Number of forks": repo.forks,
            "Number of stars": repo.stargazers_count,
            "Topics": repo.get_topics(),
            "Labels": [i._rawData for i in repo.get_labels()],
            "Contributors": [i._rawData for i in repo.get_contributors()],
            "Contributors Count": repo.get_contributors().totalCount,
            #"Subscribers": [i._rawData for i in repo.get_subscribers()],
            "Subscribers Count": repo.get_subscribers().totalCount,
            #"Watchers": [i._rawData for i in repo.get_watchers()],
            "Watchers Count": repo.get_watchers().totalCount
           }

# get top 100 repo by stars
top_repo = []
for i, repo in enumerate(all_repo):
    top_repo.append(repo)
    if i == 990:
        break

# add more fields
from tqdm.notebook import tqdm
repo_list = []
for repo in tqdm(top_repo, total=len(top_repo)):
    repo_list.append(get_repo_dict(repo))