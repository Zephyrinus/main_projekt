# create virtual env that is not be pushed to repo (invis direct)
virtualenv ~/.venv
# Access it
source ~/.venv/bin/activate

# See all version installed /lib
pip freeze | less
