from vali import more_vali, more_ciao


def test_more_vali():
    assert "Ave" == more_vali()


def test_more_ciao():
    assert "ciao" == more_ciao()
